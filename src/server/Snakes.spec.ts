import Snakes from './Snakes';
import { ISnake } from '../types';

describe('Snake', () => {
  it('init snake + tick', () => {
    const snakes = new Snakes();
    expect(snakes.getSnakes()).toHaveLength(0);
    const id = snakes.addSnake();
    expect(id).not.toBe('');
    const snake1 = snakes.getSnake(id) as ISnake;
    expect(snake1).not.toBeUndefined();
    expect(snakes.setDirection(id, 'up')).toBeTruthy();
    snakes.tick();
    const snake2 = snakes.getSnake(id) as ISnake;
    expect(snake2).not.toBeUndefined();
    expect(snake1.coordinates[0].y).toBe(snake2.coordinates[0].y);
    expect(snake1.coordinates[0].x).not.toBe(snake2.coordinates[0].x);
    expect(snakes.setDirection(id, 'left')).toBeTruthy();
    snakes.tick();
    const snake3 = snakes.getSnake(id) as ISnake;
    expect(snake3).not.toBeUndefined();
    expect(snake3.coordinates[0].y).not.toBe(snake2.coordinates[0].y);
    expect(snake3.coordinates[0].x).toBe(snake2.coordinates[0].x);
  });
});
